$(document).ready(function () {

    // Kun klikataan listalla olevaa nimeä, asetetaan se tämän hetkisesti kappaleeksi
    uploadSongList();
    $('li').click(function () {
        setSong($(this).text());
    })

    setVolume();
    setCurrentTime();
});
// Asetetaan volumeksi käyttäjän input volume sliderissa

function setVolume() {
    let slider = document.getElementById("volumecontroller");

    song.volume = slider.value / 100; // Tämä rivi mahdollistaa volumen säilymisen ennallaan, kun biisiä vaihdetaan

    slider.oninput = function () {
        song.volume = this.value / 100;
    };
}

// Asetetaan kappaleen soittokohdaksi käyttäjän input timeline sliderissa

function setCurrentTime() {
    let slider = document.getElementById("timeline")
    slider.oninput = function () {
        song.currentTime = ((this.value) / 100) * song.duration;
    }

}

// Kovakoodatut kappaleet

var song = new Audio('audio/internet_connection.mp3');
var song2 = new Audio('audio/Zander Noriega - Ironbound.mp3');
var song3 = new Audio('audio/little town - orchestral.ogg');
var songList = [];
songList.push(song);
songList.push(song2);
songList.push(song3);

function playSong() {
    song.play();
}
function pauseSong() {
    song.pause();
}

function stopSong() {
    song.pause();
    song.currentTime = 0;
}

// Lisätään kovakoodatut kappaleet biisilistaan ja listassa näytetään vain biisin tiedostonimi

function uploadSongList() {
    songList.forEach(function (element) {
        var node = document.createElement("LI");
        // Otetaan vain URI:n loppu eli tiedostonimi
        var textNode = document.createTextNode(decodeURI(element.src.split("audio/").pop()));
        node.appendChild(textNode);
        document.getElementById("songList").appendChild(node);
    });
};

// Tätä kutsutaan, kun klikataan jotain listalla olevaa biisiä

function setSong(newSong) {

    // Ensin vanha biisi pitää pysäyttää
    stopSong();

    // Uusi biisi saadaan parametrina
    song = new Audio('audio/' + newSong);

    setVolume();
    playSong();

    //Päivitetään biisin nimi kohtaan, jossa näkyy tällä hetkellä soiva biisi
    document.getElementById("currentSong").innerHTML = newSong;

    updateCurrentTime(song);
    getDuration(song);

}

// Haettu ja vähän muunneltu https://www.tutorialspoint.com/How-to-convert-seconds-to-HH-MM-SS-with-JavaScript 

var convertSeconds = function (sec) {
    var hrs = Math.floor(sec / 3600);
    var min = Math.floor((sec - (hrs * 3600)) / 60);
    var seconds = sec - (hrs * 3600) - (min * 60);
    seconds = Math.round(seconds);

    var result = (hrs < 10 ? "0" + hrs : hrs);
    result += ":" + (min < 10 ? "0" + min : min);
    result += ":" + (seconds < 10 ? "0" + seconds : seconds);
    return result;
};

// Tässä funktiossa päivitetään käyttäjälle näkyvää biisin kulunutta aikaa sekunteina
// sekä timeline sliderissa.

function updateCurrentTime(song) {
    // Seuraava rivi siksi, että currentTime ja duration päivittyvät
    // näytölle samanaikaisesti
    document.getElementById("currentTime").innerHTML = convertSeconds(song.currentTime);
    let slider = document.getElementById("timeline");

    song.ontimeupdate = function () {
        document.getElementById("currentTime").innerHTML = convertSeconds(song.currentTime);
        slider.value = Math.round((song.currentTime / song.duration) * 100);
    };
};

// Näytetään käyttäjälle biisin kokoaisaika

function getDuration(song) {
    $(song).on("loadedmetadata", function () {
        document.getElementById("duration").innerHTML = "/" + convertSeconds(song.duration);
    });
};