const net = require("net");

const client = new net.Socket();
const port = 8888;
const host = "127.0.0.1";

client.setEncoding("utf-8");

var stdin = process.openStdin();

client.connect(port, host, function() {
    console.log("CONNECTED TO HOST: " + host + " PORT: " + port);
    client.write("Uusi käyttäjä liittyi " + client.remoteAddress);

});

client.on("data", function(data) {
    console.log(data);
});

client.on("close", function() {
    console.log("Yhteys katkesi.");
    process.exit();
});

/*
 * Tämä funktio lukee käyttäjän syötettä komentoriviltä ja lähettää sen palvelimelle trimmattuna.
 */
stdin.addListener("data", function(data) {
    if (data.toString() === "@lopeta") {
        client.end();
    } else {
        client.write(data.toString().trim());
    }
    
});