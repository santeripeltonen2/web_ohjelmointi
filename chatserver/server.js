const net = require('net');

let sockets = [];

function cleanInput(data) {
	return data.toString().replace(/(\r\n|\n|\r)/gm,"");
}

/*
 * Vastaanotetaan viestejä
 */

function receiveData(socket, data) {
    var cleanData = cleanInput(data);
    /*
     * Jos käyttäjä syöttää "@lopeta", niin poistetaan käyttäjä palvelimelta
     */
    if (cleanData === "@lopeta") {
        console.log("Käyttäjä " + socket.remoteAddress + " poistui");
        socket.end("Heihei!\n");
    }
    /*
     * Yksinkertainen ja hyvin rajattu ohje yksityisviestin kirjoittamiselle 
     */
    else if (cleanData === "@help") {
        socket.write("Yksityisviestiä kirjoittaessa kirjoita @msg [käyttäjän numero] [viesti]\n" + 
        "Esimerkiksi @msg 0 moi");
    }
    /*
     * Tulostaa vain paikallaolijoiden numeron sekä remoteAddressin
     * vain käyttäjän chat-ikkunaan. Ei tulosta palvelimelle 
     * eikä muille käyttäjille. 
     */
    else if (cleanData === "@whois") {
        socket.write("Paikalla olevat käyttäjät: ");
        sockets.forEach(function(element, index) {
            socket.write("Nr." + index.toString() + " " + element.remoteAddress + "\n");
        });
    }
    /*
     * Yksityisviestin lähettäminen. Ensin tarkastetaan, jos käyttäjän viesti alkaa kirjaimilla
     * "@msg" ja sitten ohjataan viesti oikealle käyttäjälle käyttäen numeroa. Erittäin rajattu ja 
     * herkästi rikki menevä funktio.
     */
    else if (cleanData.startsWith("@msg")) {
        var userNumber = cleanData.substring(5, 6);
        sockets[userNumber].write("Yksityisviesti käyttäjältä " + socket.remoteAddress + ": " + cleanData.substring(7));
    }

    /*
     * Jos käyttäjän viesti ei ollut mikään yllä olevista, luetaan se normaalina viestinä ja tulostetaan
     * se palvelimen ikkunaan sekä kaikille muille käyttäjille paitsi itselleen. Tähän voisi vielä lisätä
     * kohdan, missä ilmenee viestin lähettäjän, mutta se vaatisi käyttäjänimien käytön.
     */
    else {
        console.log(cleanData);
        for(var i = 0; i < sockets.length; i++) {
            if (sockets[i] !== socket) {
                sockets[i].write(data);
            }
        }
    }
}

/*
 * Uusi yhteys asiakkaaseen socketin avulla ja tervehdys-viestin lähettäminen
 */
function newSocket(socket) {
    sockets.push(socket);
    socket.write("Tervetuloa chat-sovellukseen\n");
    socket.on("data", function(data) {
        receiveData(socket, data);
    })
    socket.on("end", function() {
        closeSocket(socket);
    })
}

/*
 * Poistetaan socketti, kun asiakas poistuu palvelimelta
 */
function closeSocket(socket) {
    var i = sockets.indexOf(socket);
    if (i != 1) {
        sockets.splice(i, 1);
    }
}



var server = net.createServer(newSocket);

server.listen(8888, "127.0.0.1");
console.log("palvelin käynnistetty");