function loadApp() {
  console.log("loading application");

  // Bind AJAX call to the click event of Button #lataa
  $('#lataa').click(function(event) {
    // TODO: selvitä mikä merkitys on ao. rivillä
    // Estää buttonin oletus-eventin, jolloin voidaan tehdä buttonille custom-event
    // näin huonosti suomennettuna
    event.preventDefault();

    // The server must be bind to localhost (for testing) as we don't have a FQDN or HTTP proxy available
    //TODO: correct URI
    $.get("/../server", function(data) {
      console.log("Sending HTTP GET to server");
    })
    .done(function( data ) {
      console.log("response from server :", data);
      //TODO: esitä vastaanotettu data webbisivulla, eli 
      // rakenna tässä kohtaa taulukko table+th+tr+td-elementein
      // palvelimelta tuleva vastaus on data-muuttujassa JavaScript-oliona

      // Järjestetään presidentit virkakauden mukaan vanhimmasta alken
      data.sort(function(a, b) {
        return a.career.substring(0, 4) - b.career.substring(0, 4);
      });
        
      // Lisätään tableen rivi, ja riville lisätään presidentin tiedot.
      for (var i = 0; data.length; i++) {
        var row = $("<tr>");
        row.append($("<td>" + data[i].firstname + "</td>"))
           .append($("<td>" + data[i].lastname + "</td>"))
           .append($("<td>" + data[i].nickname + "</td>"))
           .append($("<td>" + data[i].born + "</td>"))
           .append($("<td>" + data[i].died + "</td>"))
           .append($("<td>" + "<img src=" + data[i].img + " alt=kuva/>" + "</td>"))
           .append($("<td>" + data[i].career + "</td>"))
           .append($("<td>" + "<a href=" + data[i].biography +">Biografia</" +  "</td>"));
        
        $("#presidentit_taulukko").append(row);   
      };
    })
    .fail(function(err) {
      console.log("error");
    })
    .always(function() {
      console.log("finished");
    });
  });
}