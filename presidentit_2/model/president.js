var mongoose  = require('mongoose');
var Schema = mongoose.Schema;
// TODO: define the president schema
// i.e. structure of the MongoDB document
var President = new Schema({
  firstname : { 
    type: String,
    required: true, 
    max: 50
  },
  lastname : { 
    type: String,
    required: true, 
    max: 50
  },
  nickname : { 
    type: String,
    required: true, 
    max: 15, 
    validate: function(nickname) {
      return /[A-Za-z]/.test(nickname);
    }
  },
  born : {
    type: Number,
    require : true
  },
  died : {
    type: Number,
    required : false
  },
  img : {
    type: String,
    required : true
  },
  career : {
    type: String,
    required: true
  },
  biography : {
    type: String,
    required: true
  }
  // TODO: lisää loput presidentteihin liittyvät asiat
});

// create the model for presidents and expose it to the application
module.exports = mongoose.model('President', President);