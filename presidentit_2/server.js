const express = require('express');
//const Database = require('./database.js');
const ArrayList = require('Arraylist');

//const connection = new Database();

const app = express();
const port = 8000; 

var cors = require('cors');

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/presidents');

var President = require("./model/president.js");

// TÄLLÄ VOI LUODA UUDEN PRESIDENTIN¨//
/*
var sauli = new President();

  sauli.firstname = "Sauli";
  sauli.lastname = "Niinistö";
  sauli.nickname = "Sale";
  sauli.born = "1948";
  sauli.died = "";
  sauli.img = "https://kansallisbiografia.fi/img/kb/0882101.jpg";
  sauli.career = "2012 - ";
  sauli.biography = "https://kansallisbiografia.fi/kansallisbiografia/henkilo/8821";

sauli.save(function (err, sauli) {
  if (err) return console.error(err);
  console.log("Presidentti tallennettu");
});
*/

// CORS-middlewaren käyttöönotto
app.use(cors());
// Sallitaan pääsy selaimen tarvitsemiin tiedostoihin

app.use(express.static(__dirname+'/client')); 

// Kun asiakkaalta tulee GET /server pyyntö, haetaan databasesta presidenttien tiedot 
// ja lähetetään ne asiakkaalle
app.get('/server', (req, res) => {
  var data = new ArrayList;
  President.find(function (err, presidents) {

    if (err) return console.error(err);  

    data.add(presidents);
    //Lähettää datan asiakkaalle json-muodossa
    res.json(data);
  });
});

app.listen(port, '0.0.0.0', () => console.log(`Palvelin kuuntelee porttia ${port}!`));