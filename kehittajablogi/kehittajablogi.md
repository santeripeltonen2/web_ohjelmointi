# **Kehittäjäblogi**

---
# Ensimmäisen viikon tehtävät

Ensimmäisen viikon presidentit_1 tehtävä meni ihan mallikkaasti. Aikaa kului usea tunti kun sai rauhassa tutkiskella uutta kieltä ja sen toimintoja. Ongelmia oli luokkien periytyvyyksien kanssa, mutta lopulta sain kuitenkin luokat periytymään oikein mallikkaasti ja testasin toimivuuden testiohjelmalla. Koska ennen en ollut käyttänyt gititä, meni sen kanssa värkätessä jonkun aikaa kunnes hoksasin mitä täytyy tehdä ja sain sinne uploadattua tiedostot onnistuneesti. Eiköhän ensi kerralla jo gitin käyttö suju paremmin. Luokkien tekeminen node.js:llä oli mielestäni mukavaa, koska rakenne on hyvin sama kuin Javassa, eikä syntaksikaan ollut vaikea. Tästä on hyvä jatkaa.


# Toisen viikon tehtävät

Toisen viikon tehtävä oli mielenkiintoinen ja mukava koodata. Olen tehnyt aiemmin hyvin samantyyppisen chat-sovelluksen Javalla viime vuonna. Node.js:llä oli mielestäni hieman helpompi tehdä chat-palvelin, vaikkakin aikaa meni. Aluksi tein yksinkertaisen palvelimen ja liityin siihen telnetin avulla, mutta ilmeisesti Windowsini ja telnet eivät halunneet toimia keskenään, vaan viestit lähtivät palvelimelle kirjain kerrallaan.
Ilmeisesti muillaki on ollut samanlaisia ongelmia Windowsilla. [Kuva](https://i.imgur.com/UFAbhxC.jpg)
https://stackoverflow.com/questions/36878003/telnet-disable-sending-each-character
Linuxia en lähtenyt asentelemaan enää, mutta uskon, että sillä tämä olisi toiminut hyvin. 

Päätin siis tehdä itse asiakkaan, jolla voi liittyä palvelimelle. Asiakkaan teko onnistui ihan mallikkaasti, mutta valitettavasti en saanut luotua asiakkaalle käyttäjänimeä jostain kumman syystä. Ohjelma ei jäänyt odottamaan käyttäjän syötettä, vaan suorittui eteenpäin ja meni sekaisin.
[Kuva](https://i.imgur.com/9WlFQ93.jpg)

Parin kolmen tunnin jälkeen olin kokeillut jo promiseakin tuloksetta ja päätin että siirryn eteenpäin ja jätän käyttäjänimen valitsemisen tekemättä.
Käyttäjänimi-koodi näytti viimeisen kerran tältä. Varmaan parempi olisi tehdä alusta asti käyttäjän syötteen lukeminen, sillä tämä ei ole enää kovin selvää koodia. Yritin kuitenkin.
``` JavaScript
const rl = readline.createInterface( {
    input: process.stdin,
    output: process.stdout
});
function askUsername() {
    return new Promise((resolve) => {
        rl.question("Kerro käyttäjänimesi ", (username) => resolve(username));
        var username = username;
        console.log("Tervetuloa ${value}");
    });
}

rl.question("Kerro käyttäjänimesi ", (value) => {
    var username = value;
    console.log("Tervetuloa ${value}");
    rl.close;
});
```
Tällä hetkellä pystyn liittymään samaan chat-palveluun usealla eri cmd-ikkunalla. Pystyn viestittelemään, tulostaa paikallaolijat sekä lähettämään yksityisviestejä. En jaksanut enää lähteä yrittämään istunnon nicknamen muuttamista, mutta olen tyytyväinen tämän hetkiseen ohjelmaan. Käyttäjien erittely on aika hankalaa localhostina, koska remoteAddress on kaikilla sama, mutta annoin käyttäjille numerot liittymisjärjestyksen mukaan ja numeron avulla voi lähettää yksityisviestejä.

[Kuvassa](https://i.imgur.com/lwXyx27.jpg)
näkyy, että viestit menevät perille, palvelimella olevat käyttäjät tulostuvat ja yksityisviestit menevät vain tarkoitetulle asiakkaalle.

Quakenettiin liittyminen telnetin avulla onnistui hyvin.

[Part 1](https://i.imgur.com/xJW9d78.jpg)
[Part 2](https://i.imgur.com/JPmo5XG.jpg)

Komennot mitä käytin:

```
telnet irc.quakenet.org 6667
USER username 0 * :Santeri Peltonen
NICK SanteriP
PONG :3209234446
JOIN #kareliatiko
PRIVMSG #kareliatiko :Moro!
PART #kareliatiko : Terve
QUIT :Heippa
```

#### ITSEARVIOINTI
Tehtävistä 1 ja 2 suoriuduin mielestäni hyvin, vaikkakin Windowsini ja telnet eivät halunneet täysin toimia keskenään. Tekemäni asiakas kuitenkin toimii hyvin palvelimen kanssa localhostissa.

Tehtävä 3 jäi vajaaksi, kun en saanut istunnon nicknamea muutettua, mutta muuten kaikki komennot toimivat hyvin. Komentojen tekeminen oli aika suoraviivaista ja helppoa tehdä oikeat toiminnot komennoille. Opettelin samalla käyttämään hyödyllistä forEachia node.js:ssä.

Asiakkaaseen liittyen piti hetki miettiä, että miten saan ohjelman lukemaan käyttäjän syötettä ja lähettämään sen palvelimelle, mutta siitä tuli lopulta hyvin yksinkertainen funktio, mutta muilta osin asiakas-moduuli (onkohan oikea termi?) on yksinkertainen.

```JavaScript
var stdin = process.openStdin();

stdin.addListener("data", function(data) {
    if (data.toString() === "@lopeta") {
        client.end();
    } else {
        client.write(data.toString().trim());
    }
    
});
```

Tehtävä 4 eikä siinä ollut ongelmia, koska oli hyvät ohjeet.

Tehtävä 5:sta en lähtenyt tekemään, koska en saanut käyttäjänimeä asiakkaalle luotua.

Ymmärrän nyt yksinkertainen palvelimen toiminnan node.js:llä ja palvelin-asiakas ohjelmaa saisi varmasti paranneltua paljonkin sekä jonkun graafisen käyttöliittymän rakentaminen sen ympärille toisi varmasti hyvää harjoitusta.

https://gitlab.com/santeripeltonen2/web_ohjelmointi/tree/master/chatserver


# Kolmannen viikon tehtävä

Kolmannen viikon tehtävä oli mielestäni mukava tehdä, vaikka alku vaikuttikin epätoivoiselta. Alussa koodaaminen oli aikamoista sekoilua, kun mikään ei tuntunut toimivan ja tietokantakaan ei toiminut, koska en saanut mongoa kunnolla asennettua aluksi, mutta uudelleenasennus ratkaisi ongelman. Tällä kertaa Googlesta tulleet neuvot eivät auttaneet kovinkaan pitkälle, vaan täytyi itse hoksata serveripuolen logiikka. Logiikan ymmärtäessäni serverin teko oli mukavaa ja se onnistui lopulta suhteellisen helposti. Toki aikaa kului paljon, mutta sehän ei minua haitannut. 

Ensimmäinen haaste oli saada tulostettua tietokannassa olevat presidentit serverin cmd-ikkunaan. Kun olin saanut presidentit onnistuneesti tulostettua, presidentin tallennus tietokantaan oli helppoa mongoose -apin avulla. Database.js -tiedosto ei lopullisessa versiossa ole käytössä, mutta siitä oli kuitenkin jonkin verran apua. 

Seuraava ongelma serveripuolella oli saada GET -request toimimaan kunnolla. Mietin kauan ja kokeilin eri vaihtoehtoja, että mikä URI pitää olla GET-requestissa ja sekin selvisi lopulta. Tässä kohtaa olin ladannut Postman -sovelluksen, jolla pystyy helposti kokeilemaan httprequesteja. 

[Postman](https://i.imgur.com/47h9K9V.jpg)

Serveripuolen GET-request funkkari näyttää yksinkertaisuudessaan lopulta tältä:

```JavaScript
app.get('/server', (req, res) => {
  var data = new ArrayList;
  President.find(function (err, presidents) {

    if (err) return console.error(err);  

    data.add(presidents);
    //Lähettää datan asiakkaalle json-muodossa
    res.json(data);
  });
});

```

Presidentin lisäämisen kovakoodasin ja kommentoin server.js -tiedostoon, mutta en alkanut sitä sen kummemmin liittämään käyttöliittymään. Presidentin lisääminen onnistuu kuitenkin melko kätevästi cmb:llä mongo-tietokannan sisällä db.presidents.insert(...) avulla.

Asiakkaan tekeminen sujuikin sitten nopeammin, kun oli enemmän perillä asioista ja oikeasta URI:sta. Presidenttien järjestäminen vanhimmasta virkakaudesta tuoreimpaan oli helppoa pienen googlailun jälkeen, ja samaa sort -funktiota uskon tarvitsevani tulevaisuudessa. 

Päätin kovakoodata tablen valmiiksi html-tiedostoon ja client -tiedostossa vain lisään taulukkoon uusia rivejä tietokannasta tulleen järjestetyn ArrayListin mukaan. Melko kätevää mielestäni. Tässä vielä uusien rivien lisääminen taulukkoon.

```JavaScript
for (var i = 0; data.length; i++) {
        var row = $("<tr>");
        row.append($("<td>" + data[i].firstname + "</td>"))
           .append($("<td>" + data[i].lastname + "</td>"))
           .append($("<td>" + data[i].nickname + "</td>"))
           .append($("<td>" + data[i].born + "</td>"))
           .append($("<td>" + data[i].died + "</td>"))
           .append($("<td>" + "<img src=" + data[i].img + " alt=kuva/>" + "</td>"))
           .append($("<td>" + data[i].career + "</td>"))
           .append($("<td>" + data[i].biography + "</td>"));
        
        $("#presidentit_taulukko").append(row);   
      };
```

index.html:ään ei tullut muita lisäyksiä, kuin:
```html
<table id="presidentit_taulukko"> 
        <thead>
          <tr>
            <th>Etunimi</th>
            <th>Sukunimi</th>
            <th>Kutsumanimi</th>
            <th>Syntymävuosi</th>
            <th>Kuolinvuosi</th>
            <th>Kuva</th>
            <th>Virkakausi</th>
            <th>Elämänkerta</th>
          </tr>
        </thead>  
      </table>
```

Lisäsin pientä tyylittelyä taulukkoon, mutta en alkanut sitä sen kummemmin hiomaan paremman näköiseksi.

#### ITSEARVIOINTI

Tehtävä tuntui aluksi vaikealta, miltei ylitsepäätemättömältä, mutta kun aloin tarkastelemaan valmista koodia ja tehtävänantoa, niin pikkuhiljaa alkoi muodostumaan toimivaa koodia ja palaset alkoivat loksahtelemaan paikoilleen. Alussa sekoilin mongodb:n kanssa, kun en meinannut saada siihen yhteyttä ja tuli kokeiltua kaikennäköisiä funkkareita. Postman -sovellus auttoi ymmärtämään httprequesteja. 

Tämän viikon tehtävä todellakin auttoi ymmärtämään REST-rajapintaa ja tästä tehtävästä oli suuresti hyötyä. Sovellus toimii tällä hetkellä, toki rajoitteita on ja presidentin lisääminen on aika vaivalloista. Toteutin jokaisen kohdan tehtävänannosta, ainakin mielestäni, ja sain docker-kontinkin toimimaan mainiosti. 

Taulukossa tyhjät arvot näytetään nulleina. Yritin pikaisesti saada nulleja piiloon, mutta en löytänyt hyvää ratkaisua nopeasti.

[Toimii](https://i.imgur.com/KncWs3O.jpg)

https://gitlab.com/santeripeltonen2/web_ohjelmointi/tree/master/presidentit_2



# Neljännen viikon tehtävä

Neljännen viikon tehtävän aloitin laiskuuden ja muiden kiireiden vuoksi valitettavasti hieman liian myöhään. Ohjelman sain "valmiiksi" vajaa pari tuntia ennen deadlinea, mutta olen tyytyväinen aikaansaannokseeni. Tämän viikkoinen ohjelma vaati reilusti eniten työtunteja muihin tehtäviin verrattuna. Työtunteja meni yhteensä noin 20 - 30. Käyttöliittymäkomponentteja en alkanut hiomaan kovinkaan paljoa, mutta käyttöliittymä näyttää ihan siedettävältä mielestäni. Palvelinpuolta en valitettavasti kerennyt tekemään, mutta aion tehdä sen vapaa-ajallani, jolloin voisin viedä useamman musiikkitiedoston käyttöliittymään ja samalla viedä projektia eteenpäin. 

Tässä tehtävässä en oikeastaan suoraan copypastannut kuin yhden funktion, jota muutin hieman tarpeisiini soveltuvammaksi. Monet funktiot syntyivät oman ajattelun tuloksena ja monia eri vaihtoehtoja tuli taas kokeiltua. 

Html- ja CSS-tiedostojen tekemiseen en kauaa aikaa käyttänyt, vaan suurin osa ajasta meni logiikan hiomiseen JS:llä. Bootstrap -elementeistä en ollut kauhean kiinnostunut, mutta tein jumbotronin ja pari containeria Bootstrapin avulla. HTML-koodia ja containereita saisi varmasti siistittyä, mutta se jäänee myöhemmäksi. Mielestäni pääasia on, että toiminnot pelaavat. Ainoa inhottava bugi, minkä löysin, tapahtuu kun ensimmäisenä ilman biisin valitsemista painaa play, niin ensimmäinen kovakoodattu biisi pärähtää soimaan. Tämän saisi estettyä palvelimen avulla, joka tarjoilee biisejä käyttäjälle, joten ei tarvitsisi kovakoodata biisejä. 

Ajan näyttäminen ja sliderien teko opetti paljon siitä, miten eri elementeistä saa tietoa ulos ja miten sitä voi hyödyntää. Tästä funktiosta olen erityisen ylpeä. Tässä päivitetään <span>:a, mikä näyttää biisin tämänhetkisen ajan ja samalla liikutetaan slideria, joka ilmaisee visuaalisesti sen, kuinka paljon biisiä on kulunut.

```JavaScript
function updateCurrentTime(song) {
    // Seuraava rivi siksi, että currentTime ja duration päivittyvät
    // näytölle samanaikaisesti
    document.getElementById("currentTime").innerHTML = convertSeconds(song.currentTime);
    let slider = document.getElementById("timeline");

    song.ontimeupdate = function () {
        document.getElementById("currentTime").innerHTML = convertSeconds(song.currentTime);
        slider.value = Math.round((song.currentTime / song.duration) * 100);
    };
};
```

#### ITSEARVIOINTI
Olen saamaani ohjelmaan tyytyväinen. Aikaisemmin olisi pitänyt alottaa, jotta olisin saanut tehtyä myös palvelinpuolen. Sen sijaan, että olisin näyttänyt jäljellä olevan soittoajan, päätin näyttää tämänhetkisen soittoajan sekä biisin kokoaisajan. Jäljellä olevan soittoajan näyttäminen olisi onnistunut seuraavanlaisesti:
```JavaScript
song.duration - song.currentTime
```
Nautin tämänviikkoisesta tehtävästä ja aion jatkaa tätä miniprojektia vapaa-ajallani. Tästä tehtävästä oli myöskin hyvää harjoitusta HTML/JS/CSS osalta. Tulevaisuudessa, kunhan kerkeän, teen palvelinpuolen ohjelmalle, sekä lisäilen joitain ominaisuuksia ja teen käyttöliittymästä kauniimman näköisen. Ohjelma toimii Chromella, mutta esimerkiksi Firefoxilla jotkin ominaisuudet puuttuvat. Biisit on otettu opengameart.org.

Toteutin kohdat: Play, stop, pause, volume up and down (slider), biisilista, biisien kokonaispituus, kelaaminen (slider) ja jäljellä olevan soittoajan sijaan näytän tämän hetkisen soittojan. 13/15 pistettä

[Ei se kaunis ole, mutta toimii](https://i.imgur.com/1Ryo4sc.jpg)

https://gitlab.com/santeripeltonen2/web_ohjelmointi/tree/master/musiikkisoitin

# Viimeisen viikon tehtävä portfolio

Viimeisen viikon tehtävän tekemisen aloitin vasta palautuspäivänä laiskuuden ja muiden kiireiden vuoksi, mutta sain portfoliosta ihan näppärän näköisen nopeasti. Projektien kuvaukset ja
kommentit jäivät hieman suppeaksi, koska alkaa aivot olla aika jumissa jo tähän aikaan viikosta, mutta kirjoittelin kuitenkin jotain. Palauteosioon kirjoitin ruusuja ja vähän tulevaisuuden suunnitelmia. Aion osallistua web-ohjelmoinnin harjoitustyö -kurssille ja odotan sitä mielenkiinnolla. Kiirrettä vain pukkaa kovasti aina näin joulun alla. 

https://gitlab.com/santeripeltonen2/web_ohjelmointi