class Person {
    constructor(firstnames, lastname, nickname, year_of_birth, year_of_death) {
        this.firstnames = firstnames;
        this.lastname = lastname;
        this.nickname = nickname;
        this.year_of_birth = year_of_birth;
        this.year_of_death = year_of_death;
    
    }
    set Firstnames(firstnames) {
        this.firstnames = firstnames;
    }
    set Lastname(lastname) {
        this.lastname = lastname;
    }
    setNickname(nickname) {
        this.nickname = nickname;
    }
    setYearofbirth(year_of_birth) {
        this.year_of_birth = year_of_birth;
    }
    setYearofDeath(year_of_death) {
        this.year_of_death = year_of_death;
    }
    
    getFirstnames() {
        return this.firstnames;
    }
    getLastname() {
        return this.lastname;
    }
    getNickname() {
        return this.nickname;
    }
    getYearofbirth() {
        return this.year_of_birth;
    }
    getYearofdeath() {
        return this.year_of_death;
    }
}
module.exports = Person;