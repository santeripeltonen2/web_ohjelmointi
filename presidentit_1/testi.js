var Profession = require("./profession.js");

var sale = new Profession("Sauli", "Niinistö", "Sale", "1948", "elossa", "http://www.ts.fi/static/content/pic_5_3785796_k2662582_1200.jpg",
"2012", "vielä töissä", "https://fi.wikipedia.org/wiki/Sauli_Niinist%C3%B6");

sale.introduce();

sale.linkToImage();

sale.yearOfOfficeStart();

sale.yearOfOfficeEnd();

sale.linkToHistory();