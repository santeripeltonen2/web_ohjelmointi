var Person = require("./person.js");

class Profession extends Person {
    constructor(firstnames, lastname, nickname, year_of_birth, year_of_death, image, year_of_start,
    year_of_end, history) {
        super(firstnames, lastname, nickname,year_of_birth, year_of_death);
        this.image = image;
        this.year_of_start = year_of_start;
        this.year_of_end = year_of_end;
        this.history = history;
    }

    setImage(image) {
        this.image = image;
    }
    setYearofstart(year_of_start) {
        this.year_of_start = year_of_start;
    }
    setYearofend(year_of_end) {
        this.year_of_end = year_of_end;
    }
    setHistory(history) {
        this.history = history;
    }
    
    getImage() {
        return this.image;
    }
    getYearofstart() {
        return this.year_of_start;
    }
    getYearofend() {
        return this.year_of_end;
    }
    getHistory() {
        return this.history;
    }

    linkToImage() {
        console.log("Linkki presidentin kuvaan: " + this.image);
    }

    yearOfOfficeStart() {
        console.log("Virkakausi alkoi vuonna " + this.getYearofstart());
    }
    yearOfOfficeEnd() {
        console.log("Virkakausi loppui vuonna " + this.getYearofend());
    }
    linkToHistory() {
        console.log("Historiikki: " + this.history);
    }

    introduce() {
        console.log("Presidentti: " + this.getFirstnames() + " " + this.getLastname() + " syntyi vuonna " + 
    this.getYearofbirth() + ", aloitti presidenttikautensa vuonna " + this.getYearofstart());
    }

}

module.exports = Profession;